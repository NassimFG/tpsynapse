﻿
using MySql.Data.MySqlClient;

namespace SynapsInfo
{
    class DataBaseAccess
    {
        public static MySqlConnection getOpenMySqlConnection()
        {
            MySqlConnection msc = new MySqlConnection("Database=synapse;Data Source=localhost;User Id=root;Password=;Ssl Mode=None;charset=utf8");
            msc.Open();
            return msc;
        }
    }
}

//Avec Abstraction -> Diminue le COUPLAGE 

//public static IDbConnection Connexion { get; set; }

//public static IDbDataParameter CodeParam(string paramName, object value)
//{
//    IDbCommand commandSql = Connexion.CreateCommand();
//    IDbDataParameter parametre = commandSql.CreateParameter();
//    parametre.ParameterName = paramName;
//    parametre.Value = value;
//    return parametre;
//}