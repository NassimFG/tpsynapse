﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace SynapsInfo
{
    /*public permet un accès entre des classes de différents espace de nom  */
    public class Mission
    {
        #region Modele Objet

        public short Id { get; private set; } // Primary Key (Bd)
        public string Nom { get; set; }
        public string Description { get; set; }
        public decimal NbHeuresPrevues { get; set; }
        public Dictionary<TimeSpan, int> ReleveHoraire { get; set; }
        public Intervenant Intervenant { get; set; }//Foreign Key
        public Projet Projet { get; set; }

        #endregion

        public Mission()
        {
            Id = -1;//Identifie un contact non référencé dans la base de données
            Description = "";
            Nom = "";
            ReleveHoraire = new Dictionary<TimeSpan, int>();
        }

        #region Champs à portée classe contenant l'ensemble des requêtes d'accès aux données
        private static string _selectSql =
            "SELECT id ,  nom ,  description, nbHeuresPrevues FROM mission";

        private static string _selectByIdSql =
            "SELECT id ,  nom ,  description, nbHeuresPrevues, idIntervenant, idProjet FROM mission WHERE id = ?id ";

        private static string _updateSql =
            "UPDATE mission SET nom = ?nom, description=?description , nbHeuresPrevues=?nbHeuresPrevues  WHERE id=?id ";

        private static string _insertSql =
            "INSERT INTO mission (nom,description,nbHeuresPrevues,idIntervenant, idProjet) VALUES (?nom,?description,?nbHeuresPrevues,?idIntervenant,?idProjet)";

        private static string _deleteByIdSql =
            "DELETE FROM mission WHERE id = ?id";

        private static string _getLastInsertId =
            "SELECT id FROM mission WHERE nom = ?nom AND description=?description AND nbHeuresPrevues=?nbHeuresPrevues   ";

        private static string _getReleveByIdMission =
             "SELECT date, nbHeures FROM releve where idMission=?idMission ";

        private static string _deleteReleveByIdMission =
            "DELETE FROM releve WHERE idMission = ?idMission";

        private static string _insertUnReleveByIdMission =
            "INSERT INTO releve (idMission,nbHeures,date) VALUES (?idMission,?nbHeures,?date)";
        #endregion

        #region Méthodes d'accès aux données

        /// <summary>
        /// Valorise un objet contact depuis le système de gestion de bases de données
        /// </summary>
        /// <param name="idContact">La valeur de la clé primaire</param>
        public static Mission Fetch(int idMission)
        {
            Mission uneMission = null;
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _selectByIdSql;
            commandSql.Parameters.Add(new MySqlParameter("?id", idMission));
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                uneMission = new Mission();
                uneMission.Id = Convert.ToInt16(jeuEnregistrements["id"].ToString());//Lecture d'un champ de l'enregistrement
                uneMission.Nom = jeuEnregistrements["nom"].ToString();
                uneMission.Description = jeuEnregistrements["description"].ToString();
                string nbHeuresPrevues = jeuEnregistrements["nbHeuresPrevues"].ToString();
                uneMission.NbHeuresPrevues = Convert.ToDecimal(nbHeuresPrevues);
                Int16 idIntervenant = Convert.ToInt16(jeuEnregistrements["idIntervenant"].ToString());
                Intervenant unIntervenant = Intervenant.Fetch(idIntervenant);
                uneMission.Intervenant = unIntervenant;
                Int16 idProjet = Convert.ToInt16(jeuEnregistrements["idProjet"].ToString());
                Projet unProjet = Projet.Fetch(idProjet);
                uneMission.Projet = unProjet;
            }
            openConnection.Close();
            return uneMission;
        }

        /// <summary>
        /// Sauvegarde ou met à jour une Mission dans la base de données
        /// </summary>
        public void Save()
        {
            if (Id == -1)
            {
                Insert();
            }
            else
            {
                Update();

            }
            SaveReleveByIdMission(Id, ReleveHoraire);
        }

        /// <summary>
        /// Supprime le contact représenté par l'instance courante dans le SGBD
        /// </summary>
        public void Delete()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _deleteByIdSql;
            commandSql.Parameters.Add(new MySqlParameter("?id", Id));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            Id = -1;
            openConnection.Close();
        }

        private void Update()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(new MySqlParameter("?id", Id));
            commandSql.Parameters.Add(new MySqlParameter("?nbHeuresPrevues", NbHeuresPrevues));
            commandSql.Parameters.Add(new MySqlParameter("?nom", Nom));
            commandSql.Parameters.Add(new MySqlParameter("?description", Description));
            commandSql.Parameters.Add(new MySqlParameter("?idIntervenant", Intervenant.Id));
            commandSql.Parameters.Add(new MySqlParameter("?idProjet", Projet.Id));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            openConnection.Close();

        }

        private void Insert()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _insertSql;

            commandSql.Parameters.Add(new MySqlParameter("?nbHeuresPrevues", NbHeuresPrevues));
            commandSql.Parameters.Add(new MySqlParameter("?nom", Nom));
            commandSql.Parameters.Add(new MySqlParameter("?description", Description));
            commandSql.Parameters.Add(new MySqlParameter("?idIntervenant", Intervenant.Id));
            commandSql.Parameters.Add(new MySqlParameter("?idProjet", Projet.Id));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();

            Id = Convert.ToInt16(commandSql.LastInsertedId);
            openConnection.Close();
        }

        /// <summary>
        /// Retourne une collection contenant les contacts
        /// </summary>
        /// <returns>Une collection de contacts</returns>
        public static List<Mission> FetchAll()
        {
            List<Mission> resultat = new List<Mission>();
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _selectSql;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Mission uneMission = new Mission();
                string idMission = jeuEnregistrements["id"].ToString();
                uneMission.Id = Convert.ToInt16(idMission);
                uneMission.Nom = jeuEnregistrements["nom"].ToString();
                string nbHeuresPrevues = jeuEnregistrements["nbHeuresPrevues"].ToString();
                uneMission.NbHeuresPrevues = Convert.ToDecimal(nbHeuresPrevues);
                uneMission.ReleveHoraire = GetReleveByIdMission(uneMission.Id);
                resultat.Add(uneMission);
            }
            openConnection.Close();
            return resultat;
        }

        private static Dictionary<TimeSpan, int> GetReleveByIdMission(int idMission)
        {
            Dictionary<TimeSpan, int> releve = new Dictionary<TimeSpan, int>();
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _getReleveByIdMission;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                string date = jeuEnregistrements["date"].ToString();
                Int32 dateReleve = Convert.ToInt32(date);
                string nbHeures = jeuEnregistrements["nbHeures"].ToString();
                int nbHeureReleve = Convert.ToInt32(nbHeures);
                releve.Add(new TimeSpan(dateReleve), nbHeureReleve);
            }
            openConnection.Close();

            return releve;
        }

        private static void SaveReleveByIdMission(int idMission, Dictionary<TimeSpan, int> releve)
        {
            DeleteReleveByIdMission(idMission);
            foreach (KeyValuePair<TimeSpan, int> kvp in releve)
            {
                InsertReleveByIdMission(idMission, kvp);
            }
        }

        private static void DeleteReleveByIdMission(int idMission)
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _deleteReleveByIdMission;
            commandSql.Parameters.Add(new MySqlParameter("?idMission", idMission));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            openConnection.Close();
        }

        private static void InsertReleveByIdMission(int idMission, KeyValuePair<TimeSpan, int> unReleve)
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _insertUnReleveByIdMission;
            commandSql.Parameters.Add(new MySqlParameter("?date", unReleve.Key.Ticks));
            commandSql.Parameters.Add(new MySqlParameter("?nbHeures", unReleve.Value));
            commandSql.Parameters.Add(new MySqlParameter("?idMission", idMission));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            openConnection.Close();
        }

        #endregion







        /// <summary>
        /// Compare un objet transmis en paramètre avec l'objet courant. La comparaison est effectuée sur l'identifiant de l'objet.
        /// </summary>
        /// <param name="obj">L'objet à comparer (seul un objet de type Mission sera comparé avec la mission courante</param>
        /// <returns>Vrai si l'identifiant correspond, faux dans le cas contraire</returns>
        public override bool Equals(object obj)
        {
            bool equals = false;
            Mission m = obj as Mission;
            if (m != null)
            {
                if (m.Id == Id)
                {
                    equals = true;
                }
            }
            return equals;
        }


       





    }
}
