﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace SynapsInfo
{
    public class Projet
    {
        

        public short Id { get; private set; }
        public DateTime DateDebutProjet { get; set; }
        public DateTime DateFinProjet { get; set; }
        public decimal PrixFactureMO { get; set; }
        public string NomProjet { get; set; }
        public Mission Mission { get; set; }
        List<Mission> listeMissions = new List<Mission>();

        public Projet()
        {
            Id = -1;//Identifie un contact non référencé dans la base de données
            DateDebutProjet = System.DateTime.Now;
            DateFinProjet = DateDebutProjet.AddMonths(3);
            NomProjet = "";
            
            
        }

        #region Champs à portée classe contenant l'ensemble des requêtes d'accès aux données
        private static string _selectSql =
            "SELECT id ,  dateDebutProjet ,  dateFinProjet, prixFacture, nomProjet FROM projet";
        
        private static string _selectByIdSql =
            "SELECT id, dateDebutProjet, dateFinProjet, prixFacture, nomProjet FROM projet WHERE id = ?id ";
        
        private static string _updateSql =
            "UPDATE projet SET  dateDebutProjet=?dateDebutProjet, dateFinProjet=?dateFinProjet, prixFacture=?prixFacture, nomProjet = ?nomProjet WHERE id=?id ";

        private static string _insertSql =
            "INSERT INTO projet (dateDebutProjet,dateFinProjet,prixFacture,nomProjet) VALUES (?dateDebutProjet,?dateFinProjet,?prixFacture,?nomProjet)";

        private static string _deleteByIdSql =
            "DELETE FROM projet WHERE id = ?id";

        private static string _getLastInsertId =
            "SELECT id FROM projet WHERE nomProjet = ?nomProjet AND dateDebutProjet=?dateDebutProjet AND dateFinProjet=?dateFinProjet AND prixFacture=?prixFacture AND nomProjet = ?nomProjet  ";
        
        private static string _getMissionByIdProjet =
             "SELECT nom,description ,nbHeuresPrevues FROM Mission where idProjet=?idProjet ";
        /*
        private static string _deleteReleveByIdMission =
            "DELETE FROM releve WHERE idMission = ?idMission";

        private static string _insertUnReleveByIdMission =
            "INSERT INTO releve (idMission,nbHeures,date) VALUES (?idMission,?nbHeures,?date)";
            */
        #endregion

















        public static Projet Fetch(int idProjet)
        {
            Projet unProjet = null;
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _selectByIdSql;
            commandSql.Parameters.Add(new MySqlParameter("?id", idProjet));
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                unProjet = new Projet();
                unProjet.Id = Convert.ToInt16(jeuEnregistrements["id"].ToString());//Lecture d'un champ de l'enregistrement
                string dateDebutProjet = jeuEnregistrements["dateDebutProjet"].ToString();
                Int64 dateDebut = Convert.ToInt64(dateDebutProjet);
                DateTime dateDebut2 = new DateTime(dateDebut);
                //unProjet.DateDebutProjet = Convert.ToDateTime(jeuEnregistrements["dateDebutProjet"].ToString());
                string dateFinProjet = jeuEnregistrements["dateFinProjet"].ToString();
                Int64 dateFin = Convert.ToInt64(dateDebutProjet);
                DateTime dateFin2 = new DateTime(dateFin);
                //unProjet.DateFinProjet = Convert.ToDateTime(jeuEnregistrements["dateFinProjet"].ToString());
                unProjet.PrixFactureMO = Convert.ToDecimal(jeuEnregistrements["prixFacture"]);
                unProjet.NomProjet = jeuEnregistrements["nomProjet"].ToString();

            }
            openConnection.Close();
            return unProjet;
        }

        /// <summary>
        /// Sauvegarde ou met à jour une Mission dans la base de données
        /// </summary>
        public void Save()
        {
            if (Id == -1)
            {
                Insert();
            }
            else
            {
                Update();

            }
            //SaveReleveByIdMission(Id, ReleveHoraire);
        }

        /// <summary>
        /// Supprime le contact représenté par l'instance courante dans le SGBD
        /// </summary>
        public void Delete()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _deleteByIdSql;
            commandSql.Parameters.Add(new MySqlParameter("?id", Id));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            Id = -1;
            openConnection.Close();
        }

        private void Update()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(new MySqlParameter("?id", Id));
            commandSql.Parameters.Add(new MySqlParameter("?dateDebutProjet", DateDebutProjet));
            commandSql.Parameters.Add(new MySqlParameter("?dateFinProjet", DateFinProjet));
            commandSql.Parameters.Add(new MySqlParameter("?nomProjet", NomProjet));
            commandSql.Parameters.Add(new MySqlParameter("?prixFacture", PrixFactureMO));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            openConnection.Close();

        }

        private void Insert()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _insertSql;

            commandSql.Parameters.Add(new MySqlParameter("?dateDebutProjet", DateDebutProjet.Ticks));
            commandSql.Parameters.Add(new MySqlParameter("?dateFinProjet", DateFinProjet.Ticks));
            commandSql.Parameters.Add(new MySqlParameter("?prixFacture", PrixFactureMO));
            commandSql.Parameters.Add(new MySqlParameter("?nomProjet", NomProjet));
            
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();

            Id = Convert.ToInt16(commandSql.LastInsertedId);
            openConnection.Close();
        }

        /// <summary>
        /// Retourne une collection contenant les contacts
        /// </summary>
        /// <returns>Une collection de contacts</returns>
        public static List<Projet> FetchAll()
        {
            List<Projet> resultat = new List<Projet>();
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _selectSql;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Projet unProjet = new Projet();
                string idProjet = jeuEnregistrements["id"].ToString();
                unProjet.Id = Convert.ToInt16(jeuEnregistrements["id"].ToString());//Lecture d'un champ de l'enregistrement
                //unProjet.DateDebutProjet = Convert.ToDateTime(jeuEnregistrements["dateDebutProjet"].ToString());
                //unProjet.DateFinProjet = Convert.ToDateTime(jeuEnregistrements["dateFinProjet"].ToString());
                string dateDebutProjet = jeuEnregistrements["dateDebutProjet"].ToString();
                Int64 dateDebut = Convert.ToInt64(dateDebutProjet);
                DateTime dateDebut2 = new DateTime(dateDebut);
                
                string dateFinProjet = jeuEnregistrements["dateFinProjet"].ToString();
                Int64 dateFin = Convert.ToInt64(dateDebutProjet);
                DateTime dateFin2 = new DateTime(dateFin);
                unProjet.PrixFactureMO = Convert.ToDecimal(jeuEnregistrements["prixFacture"]);
                unProjet.NomProjet = jeuEnregistrements["nomProjet"].ToString();
                resultat.Add(unProjet);
            }
            openConnection.Close();
            return resultat;
        }







        public void AjouterMission(Mission uneMission)
        {
            bool exist = true;
            
            foreach (Mission missionCourante in listeMissions)
            {
                if (uneMission.Equals(missionCourante))
                {
                    exist = true;
                }
                else
                {
                    exist = false;
                }
                
            }
            if (exist == false)
            {
                listeMissions.Add(uneMission);
                uneMission.Projet = this;
            }
            

        }



        public void SupprimerMission(Mission uneMission)
        {
            bool resultat = false;
            foreach (Mission missionCourante in listeMissions)
            {
                if (uneMission.Equals(missionCourante))
                {
                    resultat = true;
                }

            }
            if (resultat == true)
            {
                listeMissions.Remove(uneMission);
                uneMission.Projet = null;
            }
            
        }












        public static List<Mission> GetMissionByIdProjet(Projet unProjet)
        {
            List<Mission> resultat = new List<Mission>();
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _getMissionByIdProjet;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Mission uneMission = new Mission();
                //string idMission = jeuEnregistrements["id"].ToString();
                //uneMission.Id = Convert.ToInt16(idMission);
                uneMission.Nom = jeuEnregistrements["nom"].ToString();
                string nbHeuresPrevues = jeuEnregistrements["nbHeuresPrevues"].ToString();
                uneMission.NbHeuresPrevues = Convert.ToDecimal(nbHeuresPrevues);
                resultat.Add(uneMission);
            }
            openConnection.Close();
            return resultat;
        }








    }
}
